/* eslint-disable @next/next/no-html-link-for-pages */
import styles from "./styles.module.scss";
import type { GetStaticProps, NextPage } from "next";
import { VideoItem } from "../VideoItem";
import withHeader from "../../utils/withHeader";
import { collectionDocument } from "../../core-nextv3/document/document.api";
import { AUTH_SETTING, VIDEO_SETTING } from "../../setting/setting";
import { protectedAuth } from "../../core-nextv3/auth/auth.api";
import { LoadingPage } from "../../component-nextv3/LoadingPage";

const VideosPage : NextPage<any> = ({ videos }: any) => 
{
	const loadProtectedAuth = protectedAuth(AUTH_SETTING, '', '/login');

    if(!loadProtectedAuth) 
    {
        return (<LoadingPage/>)
    }

	return (
		<div className={styles.videosPage}>
			<div className={styles.content}>
				<div className={styles.videos}>
				{videos.length >= 1 ? (
					videos.map((video: any) => (
					<VideoItem key={video.id} video={video} />
					))
				) : (
					<p>Não há videos por enquanto.</p>
				)}
				</div>
			</div>
		</div>
	);
};

const getStaticProps : GetStaticProps = () => withHeader(async (props: any) => 
{
	let videos = await collectionDocument(VIDEO_SETTING.merge({
		where: [
			{
			  field: "published",
			  operator: "==",
			  value: true,
			},
		  ],
	}));

    return {
      	props: {
			videos : videos?.collection   || [],
      	},
		revalidate : 60,
    };
});

export { getStaticProps as GetStaticProps, VideosPage}