import React, { useRef, useState } from "react";
import { useForm } from "react-hook-form";
import {
  AiOutlineCloseCircle,
  AiOutlineLeft,
  AiOutlineRight,
} from "react-icons/ai";
import Slider from "react-slick";
import { toast } from "react-hot-toast";
import styles from "./styles.module.scss";
import NumericInput from "react-numeric-input";
import { TransformWrapper, TransformComponent } from "react-zoom-pan-pinch";
import { BsZoomIn, BsZoomOut } from "react-icons/bs";
import { setItemCart, setItemsCart } from "../../core-nextv3/cart/cart.api";
import { CART_SETTING } from "../../setting/setting";
import { AnimatedLoading } from "../../component-nextv3/AnimatedLoading";
import { dispatchEvent } from "../../core-nextv3/util/use.event";
import { findDuplicates } from "../../core-nextv3/util/util";
import { ImageSet } from "../../component-nextv3/ImageSet";

export function AddToCartModal({ setModal, product }: any) 
{
    const [animateLoading, setAnimateLoading] = useState(false);
    const [variantValue, setVariantValue] = useState(product.variant ? product?.variant[0]?.items[0]?.value : '_default');
    const slider = useRef<any>(null);
    const { register, handleSubmit, setValue } = useForm<any>();

    const sliderSettings = {
        dots: false,
        infinite: true,
        speed: 500,
        slidesToShow: 1,
        slidesToScroll: 1,
        nextArrow: <AiOutlineRight />,
        prevArrow: <AiOutlineLeft />,
    };

    function getImagesVariant(i: any) {
      const images = product?.images?.data[variantValue]?.images;
      return images;
    }

  const handleAddToCart = async (formData: any) => 
  {
      setAnimateLoading(true);
      let result;

      if(product?.variant)
      {
        const variantColor = product?.variant[0]?.items?.filter(
          (color: any) => color.value == variantValue
        );
    
        const sizesQuantity = formData?.data?.filter(
          (size: any) => size != undefined
        );
    
        const totalQuantity = sizesQuantity.reduce(function (acc: any, value: any) {
          return acc + (Number(value[`${Object.keys(value)}`]?.quantity) || 0);
        }, 0);
    
        const finalQuantityTable = sizesQuantity.reduce(
          (acc: any, value: any) => ({
            ...acc,
            [`${variantColor[0].value}-${Object.keys(value)[0]}`]: {
              ...value[Object.keys(value)[0]],
            },
          }),
          {}
        );
    
        const sizesTable = product?.variant[1]?.items?.filter((size: any) => {
          for (let i = 0; i < sizesQuantity.length; i++) {
            if (Object.keys(sizesQuantity[i])[0] == size.label) {
              return size;
            }
          }
        });
    
        if (totalQuantity == 0) {
          setAnimateLoading(false);
          return toast.error("Adicione a quantidade de um produto!");
        }

        const newData = {
          data: {
            product: {
              referencePath: product.referencePath,
            },
            quantityTable: {
              data: finalQuantityTable,
              variant: [{ items: variantColor }, { items: sizesTable }],
            },
          },
        };

        result = await setItemsCart(CART_SETTING.merge(newData));
      }  
      else
      {
        const newData = {
          data: {
            product: {
              referencePath: product.referencePath,
            },
            quantity: 1,
          },
        };

        result = await setItemCart(CART_SETTING.merge(newData));
      }          
      

    if (result.status == false) {
      setAnimateLoading(false);
      return toast.error(result.error, {
        duration: 2000,
      });
    }

    setAnimateLoading(false);

    //Clean all Inputs
    document
      .querySelectorAll(".react-numeric-input input")
      .forEach((value: any) => {
        value.setValue("");
      });

    dispatchEvent("changeCart", result.data);

    toast.success("Produto adiconado com sucesso!");
  };

  const verifyProductTotalQuantity = () => {
    const obj = product?.stockTable?.data;

    let total = 0;

    for (var el in obj) {
      if (obj?.hasOwnProperty(el)) {
        total += obj[el]?.quantity;
      }
    }

    return total;
  };

  const handleQuantity = (size: any, index: any, quantity: any) => {
    if (
      quantity >
      product?.stockTable?.data?.[`${variantValue + "-" + size?.value}`]
        ?.quantity
    ) {
      return toast.error(
        `Limite de estoque é: ${
          product?.stockTable?.data?.[`${variantValue + "-" + size?.value}`]
            ?.quantity
        }`
      );
    }

    setValue(`data.${index}`, {
      [size?.value]: { quantity: quantity },
    });
  };

  const verifyVariantStock = (product: any, color: any, size: any) => 
  {
    if(!product.stockTable.data) 
    {
        return 0;
    }

    if(!product.stockTable?.data?.[`${color + "-" + size}`]) 
    {
        return 0;
    }

    return product.stockTable?.data?.[`${color + "-" + size}`]?.quantity;
  };

  return (
    <div className={styles.addToCartModal} onClick={() => setModal(false)}>
      <div className={styles.content} onClick={(e) => e.stopPropagation()}>
        <AiOutlineCloseCircle
          onClick={() => {
            setModal(false);
          }}
          className={styles.productCloseModal}
        />
        <div className={styles.productSelect}>
          <div className={styles.productImages}>
            <Slider {...sliderSettings} ref={slider} className="productSlider">
              {getImagesVariant(0)?.map((image: any, index: any) => (
                  <TransformWrapper key={index}>
                      {({ zoomIn, zoomOut, resetTransform, ...rest }) => (
                        <React.Fragment>
                          <div className={styles.tools}>
                            <a onClick={() => zoomIn()}><BsZoomIn/></a>
                            <a onClick={() => zoomOut()}><BsZoomOut/></a>
                          </div>
                            <TransformComponent>
                                <div className={styles.image}>
                                     <ImageSet image={image} width={2000}/> 
                                </div>                                
                            </TransformComponent>
                        </React.Fragment>
                      )}                        
                  </TransformWrapper>
              ))}
            </Slider>
          </div>
          <div className={styles.productInfo}>
            <p className={styles.productName}>{product?.name}</p>
            <form>
              
              {product.variant && product.variant[0] && <div className={styles.formItem}>
                <label>Selecione a cor:</label>
                <div className={styles.colorSelector}>
                  {product?.variant[0]?.items.map((color: any, index: any) => (
                    <span
                      onClick={() => {
                        slider.current.slickGoTo(0);
                        setVariantValue(color.value);
                      }}
                      className={
                        color.value == variantValue
                          ? `${styles.colorItem} ${styles.active}`
                          : `${styles.colorItem}`
                      }
                      key={index}
                    >
                      {color.label}
                    </span>
                  ))}
                </div>
              </div>}

              {verifyProductTotalQuantity() != 0 &&
              product.stockTable &&
              product.stockTable.data != "" ? (
                <>
                  <div className={styles.formItem}>
                    <label>Insira as quantidades abaixo:</label>
                    <div className={styles.formInputs}>
                      {product.variant && product.variant[1] && findDuplicates(product?.variant[1]?.items)?.map(
                        (size: any, index: any) => (
                          <div className={styles.formInputItem} key={size?.id}>
                            <span>{size?.label}</span>
                            <div className={styles.divider}></div>
                            {verifyVariantStock(
                              product,
                              variantValue,
                              size.label
                            ) == 0 ? (
                              <p className={styles.noStock}>indisponível</p>
                            ) : (
                              <NumericInput
                                {...register(`data.${index}`)}
                                style={{ width: "100%" }}
                                // format={(num: any) => Number(num)}

                                min={0}
                                max={verifyVariantStock(
                                  product,
                                  variantValue,
                                  size.label
                                )}
                                onChange={(e: any) =>
                                  handleQuantity(size, index, e)
                                }
                                mobile={true}
                              />
                            )}
                          </div>
                        )
                      )}
                    </div>
                  </div>
                </>
              ) : (
                <p className={styles.noStock}>Não há estoque</p>
              )}
            </form>
            <button type="button" onClick={handleSubmit(handleAddToCart)}>
              {animateLoading ? <AnimatedLoading /> : "Adicionar ao carrinho"}
            </button>
          </div>
        </div>
      </div>
    </div>
  );
}
