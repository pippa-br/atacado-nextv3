import styles from "./styles.module.scss";
import { useState } from "react";
import { useForm } from "react-hook-form";
import { toast } from "react-hot-toast";
import InputMask from "react-input-mask";
import { getUserAuth, setUserAuth } from "../../core-nextv3/auth/auth.api";
import IUser from "../../core-nextv3/interface/i.user";
import { AUTH_SETTING } from "../../setting/setting";
import ErrorMessage from "../../component-nextv3/ErrorMessage";
import { validateCNPJ } from "../../core-nextv3/util/validate";
import { buscaCep } from "../../core-nextv3/util/util";
import { AnimatedLoading } from "../../component-nextv3/AnimatedLoading";

export const UserForm = () => 
{
	const {
		register,
		formState: { errors },
		handleSubmit,
		watch,
		setValue,
		reset,
	} = useForm<any>();	
  	const [animateLoading, setAnimateLoading] = useState(false);
  	const [address, setAddress] = useState<any>();
	const [user, setUser] = useState<IUser>();

	getUserAuth(AUTH_SETTING, (data:any) => 
	{
		delete data.password;
		delete data.confirmPassword;

		reset(data);
		reset(data); // BUG: INPUTMASK

		setUser(data);
	});	

  	const passwordWatcher = watch("password", "");

	const onSubmit = async (data: any) => 
	{
		data.address.country = { id: "br", label: "Brasil", value: "br", selected: true };

		const newData = {
			document: {
				referencePath: user?.referencePath,
			},
			data: {
				cpfcnpj			: data.cpfcnpj,
				confirmPassword : data.confirmPassword,
				consent			: data.consent,
				email			: data.email,
				name			: data.name,
				password		: data.password,
				phone			: data.phone,
				storeName		: data.storeName,
				address			: data.address,
			},
		};		

    	await setAnimateLoading(true);

		const result = await setUserAuth(AUTH_SETTING.merge(newData));

		if(result.status === false) 
		{
			await setAnimateLoading(false);
			return toast.error(result.error);
		}

		toast.success("Dados atualizados com sucesso!");

		await setAnimateLoading(false);
  	};

  	return (
		<div className={styles.registerPage}>
		<div className={styles.content}>
			<div className={styles.right}>
			<p className={styles.title}>Atualização de cadastro</p>
			<form>
				<div className={styles.formItem}>
				<label>Nome</label>
				<input
					type="text"
					{...register("name", { required: "Este campo é obrigatório!" })}
				/>
				{errors.name && <ErrorMessage message={errors.name.message} />}
				</div>
				{/* <div className={styles.formItem}>
				<label>Sobrenome</label>
				<input
					type="text"
					defaultValue={user?.surname || ""}
					{...register("surname", {
					required: "Este campo é obrigatório!",
					})}
				/>
				{errors.surname && (
					<ErrorMessage message={errors.surname.message} />
				)}
				</div> */}
				<div className={styles.formItem}>
				<label>Celular (whatsapp)</label>
				<InputMask
					mask="(99) 99999-9999"
					maskChar=""
					{...register("phone", {
					validate: (value) =>
						value.length >= 15 || "Verifique seu celular!",
					})}
				/>
				{errors.phone && <ErrorMessage message={errors.phone.message} />}
				</div>
				<div className={styles.formItem}>
				<label>Email</label>
				<input
					type="email"
					{...register("email", {
					required: "Este campo é obrigatório!",
					})}
				/>
				{errors.email && <ErrorMessage message={errors.email.message} />}
				</div>
				<div className={styles.formItem}>
				<label>CNPJ</label>
				<InputMask
					{...register("cnpj", {
					validate: (value) => validateCNPJ(value) || "CNPJ inválido!",
					})}
					mask="99.999.999/9999-99"
					maskChar=""
				/>
				{errors.cnpj && <ErrorMessage message={errors.cnpj.message} />}
				</div>
				<div className={styles.formItem}>
				<label>Nome da loja</label>
				<input
					type="text"
					{...register("storeName", {
					required: "Este campo é obrigatório!",
					})}
				/>
				{errors.storeName && (
					<ErrorMessage message={errors.storeName.message} />
				)}
				</div>
				<div className={styles.formItem}>
				<label>CEP</label>
				<InputMask
					mask="99999-999"
					maskChar=""
					onKeyUp={(e: any) =>
					e.target.value.length === 9 &&
					buscaCep(e.target.value, setAddress, setValue)
					}
					{...register("address.zipcode", { required: "Este campo é obrigatório!" })}
				/>
				{errors.cep && <ErrorMessage message={errors?.address?.zipcode?.message} />}
				</div>
				<div className={styles.formItem}>
				<label>Endereço</label>
				<input
					type="text"
					readOnly
					{...register("address.street", {
					required: "Este campo é obrigatório!",
					})}
				/>
				{errors.street && (
					<ErrorMessage message={errors?.address?.street?.message} />
				)}
				</div>
				<div className={styles.formItem}>
				<label>Número</label>
				<input
					type="text"
					autoComplete="new-password"
					{...register("address.housenumber", {
					required: "Este campo é obrigatório!",
					})}
				/>
				{errors.housenumber && (
					<ErrorMessage message={errors.address.housenumber.message} />
				)}
				</div>
				<div className={styles.formItem}>
				<label>Complemento</label>
				<input
					type="text"
					{...register("address.complement")}
				/>
				{errors.complement && (
					<ErrorMessage message={errors.address.complement.message} />
				)}
				</div>
				<div className={styles.formItem}>
				<label>Bairro</label>
				<input				
					type="text"
					readOnly
					{...register("address.district", {
					required: "Este campo é obrigatório!",
					})}
				/>
				{errors.district && (
					<ErrorMessage message={errors.address.district.message} />
				)}
				</div>
				<div className={styles.formItem}>
				<label>Cidade</label>
				<input				
					type="text"
					readOnly
					{...register("address.city", {
					required: "Este campo é obrigatório!",
					})}
				/>
				{errors.city && <ErrorMessage message={errors.address.city.message} />}
				</div>
				<div className={styles.formItem}>
				<label>Estado</label>
				<input
					type="text"
					readOnly
					{...register("address.state", {
					required: "Este campo é obrigatório!",
					})}
				/>
				{errors.state && <ErrorMessage message={errors.address.state.message} />}
				</div>

				<div className={styles.formItem}>
				<label>Senha</label>
				<input
					type="password"
					{...register("password", {
					required: "Este campo é obrigatório!",
					minLength: {
						value: 8,
						message: "Sua senha deve possuir, no mínimo, 8 caracteres.",
					},
					})}
				/>
				{errors.password && (
					<ErrorMessage message={errors.password.message} />
				)}
				</div>
				<div className={styles.formItem}>
				<label>Confirme sua senha</label>
				<input
					type="password"
					{...register("confirmPassword", {
					required: "Este campo é obrigatório!",
					validate: (value) =>
						value === passwordWatcher ||
						"As senhas precisam ser iguais!",
					})}
				/>
				{errors.confirmPassword && (
					<ErrorMessage message={errors.confirmPassword.message} />
				)}
				</div>
			</form>

			{errors.consent && <ErrorMessage message={errors.consent.message} />}
			<div className={styles.buttons}>
				<button type="button" onClick={handleSubmit(onSubmit)}>
				{animateLoading ? <AnimatedLoading /> : "Atualizar dados"}
				</button>
			</div>
			</div>
		</div>
		</div>
  	);
};
