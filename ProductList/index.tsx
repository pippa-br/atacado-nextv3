import { ProductItem } from "../ProductItem";
import styles from "./styles.module.scss";

export const ProductList = ({ products }: any) => {
  return (
    <div className={styles.productList}>
      {products &&
        products?.map((product: any) => (
          <ProductItem key={product?.id} product={product} />
        ))}
    </div>
  );
};
