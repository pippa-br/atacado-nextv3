import styles from "./styles.module.scss";
import Select from "react-select";
import { useRouter } from "next/router";
import { useEffect, useState } from "react";

export const Filters = ({ sizes, colors, categories, onCategoryClick, onColorClick, onSizeClick }: any) => 
{
    const [defaultColorFilter, setDefaultColorFilter] = useState();
    const [defaultSizeFilter, setDefaultSizeFilter] = useState();
    const [defaultCategoryFilter, setDefaultCategoryFilter] = useState();
    const router = useRouter();
    const { query, isReady }  = useRouter();

    const getCategoryOptions = (categories: any) => 
    {
        const newOptions = categories.map((category: any) => 
        {
            return {
                value : category.name,
                label : category.name,
                model : category,
            };
        });

        return newOptions;
    };

  const customSelectStyles: any = {
    control: (base: any, state: any) => ({
      ...base,
      borderRadius: "none",
      borderColor: state.isFocused
        ? "var(--theme-color)"
        : "var(--theme-color)",
      boxShadow: state.isFocused ? "none" : null,
      "&:hover": {
        borderColor: "var(--theme-color)",
      },
      color: "#000",
      backgroundColor: state.hasValue ? "var(--theme-color)" : null,
    }),
    singleValue: (styles: any) => ({ ...styles, color: "#fff" }),
    indicatorsContainer: (styles: any, state: any) => ({
      ...styles,

      color: state.hasValue ? "#fff" : "#000",
    }),
    indicatorSeparator: () => null,
  };

    useEffect(() => 
    {
        if(isReady)
        {
            if(query.color) 
            {
                setDefaultColorFilter(
                    colors?.items.filter(
                        (color: any) => color?.value == query.color
                    )
                );  
            } 
            else 
            {
                setDefaultColorFilter(undefined);
            }

            if(query.size) 
            {
                setDefaultSizeFilter(
                    sizes?.items.filter((size: any) => size?.value == query.size)
                );
            } 
            else 
            {
                setDefaultSizeFilter(undefined);
            }

            if(query.category) 
            {              
                setDefaultCategoryFilter(
                    getCategoryOptions(
                      categories.filter(
                        (category: any) => category.name == router.query.category
                      )
                    )
                );
            } 
            else 
            {
                setDefaultCategoryFilter(undefined);
            }
        }        

    }, [isReady, query]);

  return (
    <div className={styles.container}>
      {colors && colors.items && <Select
        placeholder="Cores"
        options={colors.items}
        styles={customSelectStyles}
        isClearable
        value={defaultColorFilter || null}
        onChange={(e: any) => onColorClick(e)}
      />}
      {sizes && sizes.items && <Select
        placeholder="Tamanhos"
        options={sizes.items}
        styles={customSelectStyles}
        isClearable
        value={defaultSizeFilter || null}
        onChange={(e: any) => onSizeClick(e)}
      />}
      <Select
        placeholder="Categorias"
        options={getCategoryOptions(categories)}
        styles={customSelectStyles}
        isClearable
        value={defaultCategoryFilter}
        onChange={(e: any) => onCategoryClick(e)}
      />
    </div>
  );
};
