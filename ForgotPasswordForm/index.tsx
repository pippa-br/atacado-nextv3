import styles from "./styles.module.scss";
import { useState } from "react";
import { useForm } from "react-hook-form";
import { toast } from "react-hot-toast";
import { useRouter } from "next/router";
import { recoveryPasswordAuth } from "../../core-nextv3/auth/auth.api";
import { AUTH_SETTING } from "../../setting/setting";
import ErrorMessage from "../../component-nextv3/ErrorMessage";
import { AnimatedLoading } from "../../component-nextv3/AnimatedLoading";

export const ForgotPasswordForm = ({ account }: any) => 
{
	const router = useRouter();
	const [animateLoading, setAnimateLoading] = useState(false);
  	const [recoveryMessage, setRecoveryMessage] = useState();

	const {
		register,
		formState: { errors },
		handleSubmit,
	} = useForm();

	async function handleRecoveryPassword(formData: any) 
	{
		setAnimateLoading(true);

		const result = await recoveryPasswordAuth(AUTH_SETTING.merge(formData));

		if(result.status === false) 
		{
			setAnimateLoading(false);
			return toast.error(result.error || "Erro ao tentar recuperar a senha.");
		}

		setAnimateLoading(false);

		setRecoveryMessage(result.message);
	}

	return (
		<div className={styles.forgotPasswordPage}>
		<div className={styles.content}>
			<div className={styles.left}>
			<img
				className={styles.logo}
				src={account.logoLogin._url}
				onClick={() => router.push("/login")}
				alt="Logo"
			/>
			<p className={styles.title}>Esqueci a senha</p>
			<p className={styles.subtitle}>Digite seu e-mail</p>
			<form>
				<div className={styles.formItem}>
				<label>Email</label>
				<input
					type="email"
					{...register("login", {
					required: "Este campo é obrigatório!",
					pattern: {
						value: /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i,
						message: "Email inválido",
					},
					})}
				/>
				{errors.login && <ErrorMessage message={errors.login.message} />}
				{recoveryMessage && (
					<p className={styles.recoveryMessage}>{recoveryMessage}</p>
				)}
				</div>
			</form>
			<div className={styles.buttons}>
				<button
				type="button"
				onClick={handleSubmit(handleRecoveryPassword)}
				>
				{animateLoading ? <AnimatedLoading /> : "Recuperar a senha"}
				</button>
				<button type="button" onClick={() => router.push("/login")}>
				Fazer login
				</button>
			</div>
			</div>
			<div className={styles.right}>
			<img src={account.background_login[0]._url} alt="Imagem" />
			</div>
		</div>
		</div>
	);
};
