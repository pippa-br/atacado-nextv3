import styles from "./styles.module.scss";
import React, { useState } from "react";
import { BiCartAlt } from "react-icons/bi";
import { AddToCartModal } from "../AddToCartModal";
import { firstProductImage, productPrice } from "../../core-nextv3/product/product.util";
import { ImageSet } from "../../component-nextv3/ImageSet";
import { findDuplicates } from "../../core-nextv3/util/util";

export const ProductItem: React.FC<any> = ({ product, productInfo = true }) => 
{
  const [modal, setModal] = useState(false);

  return (
    <>
      {firstProductImage(product) ? (
        <div className={styles.productItem}>
          <div className={styles.productImg}> 
            <ImageSet
              className={styles.productImage}
              image={firstProductImage(product)}
              width={480}
              onClick={() => setModal && setModal(true)}
            />
          </div>
          <p className={styles.productName}>{product?.name}</p>
          <p className={styles.productSKU}>Referência: {product?.code}</p>
          {productInfo ? (
            <>
              {product?.variant && product?.variant[1] && <div className={styles.productSize}>
                {findDuplicates(product?.variant[1]?.items)?.map(
                  (size: any) => (
                    <p key={size.id}>{size?.label}</p>
                  )
                )}
              </div>}
              {product?.variant && product?.variant[0] &&<div className={styles.productColor}>
                {product?.variant[0]?.items?.map((color: any) => (
                  <p key={color.id}>{color.label}</p>
                ))}
              </div>}
            </>
          ) : null}

          <p className={styles.productPrice}>{productPrice(product)}</p>
          <button type="button" onClick={() => setModal(true)}>
            <BiCartAlt /> Adicionar ao carrinho
          </button>
        </div>
      ) : null}

      {modal && <AddToCartModal setModal={setModal} product={product} />}
    </>
  );
};
