/* eslint-disable @next/next/no-img-element */
import styles from "./styles.module.scss";
import type { GetServerSideProps, GetStaticProps, NextPage } from "next";
import { FaWhatsapp } from "react-icons/fa";
import ReactPlayer from "react-player";
import router from "next/router";
import { BiCart } from "react-icons/bi";
import { BsBag } from "react-icons/bs";
import { useState } from "react";
import { HiMenu } from "react-icons/hi";
import { IoMdShareAlt } from "react-icons/io";
import { AiOutlineClose } from "react-icons/ai";
import moment from "moment";
import { CartModal } from "../CartModal";
import { ProductItem } from "../ProductItem";
import { HeaderMenuModal } from "../HeaderMenuModal";
import { CountDown } from "../../component-nextv3/CountDown";
import { VideoProductsModal } from "../VideoProductsModal";
import withHeader from "../../utils/withHeader";
import { collectionDocument, getDocument } from "../../core-nextv3/document/document.api";
import { VIDEO_SETTING } from "../../setting/setting";
import { onEvent } from "../../core-nextv3/util/use.event";

const VideoPage : NextPage<any> = ({ video, cart, user, account }) => 
{
	const [openModal, setOpenModal] = useState(false);
	const [openProductsModal, setOpenProductsModal] = useState(false);
	const [openMenuModal, setOpenMenuModal] = useState(false);
	const [mobileCart, setMobileCart] = useState(cart);
	const newDate = moment(video?.date, "DD-MM-YYYY HH:mm:ss").unix();
	const date = moment(video?.date, "DD-MM-YYYY HH:mm:ss");
	const diff = moment();
	const remainSeconds = date.diff(diff, "seconds");

	onEvent("changeCart", (data:any) => {
		setMobileCart(data);
	});

	return (
		<>
		{remainSeconds >= 0 ? (
			<div className={styles.countContainer}>
			<h3>#Live em Breve!!</h3>
			<CountDown date={newDate} />
			</div>
		) : (
			<div className={styles.videoPage}>
			<div className={styles.content}>
				<div
				className={styles.player}
				style={
					video?.orientation?.value == "landscape"
					? { width: "100%" }
					: undefined
				}
				>
				<div className={styles.playerHeader}>
					<div className={styles.headerMenu}>
					<HiMenu onClick={() => setOpenMenuModal(true)} />
					<img
						onClick={() => router.push("/")}
						src={account.logo._url}
						alt="Chiclé"
					/>
					</div>
					<div className={styles.headerIcons}>
					<div>
						<AiOutlineClose onClick={() => router.back()} />
					</div>
					</div>
				</div>
				<ReactPlayer
					width="100%"
					height="100%"
					url={video?.url}
					controls
					config={{
					vimeo: {
						playerOptions: {
						playsinline: true,
						portrait: false,
						autoplay: true,
						},
					},
					youtube: {
						playerVars: {
						showinfo: 0,
						playsinline: true,
						autoplay: true,
						},
					},
					file: {
						attributes: {
						controlsList: "nofullscreen",
						},
					},
					}}
				/>
				<div className={styles.mobileOptions}>
					{/* <div
					onClick={() =>
						window.open(
						`https://api.whatsapp.com/send?phone=${video?.store?.phone}`,
						"_blank"
						)
					}
					>
					<FaWhatsapp />
					</div> */}
					<div>
					<BiCart onClick={() => setOpenModal(true)} />
					<span>{mobileCart?.items?.length}</span>
					</div>
					<div>
					<BsBag onClick={() => setOpenProductsModal(true)} />
					</div>
				</div>
				</div>
				<div className={styles.products}>
				{/* <div className={styles.top}>
					<button
					type="button"
					onClick={() =>
						window.open(
						`https://api.whatsapp.com/send?phone=${video?.store?.phone}`,
						"_blank"
						)
					}
					>
					Quero falar com a loja <FaWhatsapp />
					</button>
					<FaHeart />
					<img
					onClick={() => router.push(`/loja/${video?.store?.code}`)}
					src={video?.store?.logo?._url}
					alt=""
					/>
				</div> */}
				<div className={styles.productsList}>
					{video?.products?.map((product: any) => (
					<ProductItem
						priceViewer={video?.store?.priceViewer}
						user={user}
						key={product.id}
						product={product}
					/>
					))}
				</div>
				{/* <div className={styles.moreProducts}>
					<button
					type="button"
					onClick={() => router.push(`/loja/${video?.store?.code}`)}
					>
					Veja mais produtos da loja <FaStore />
					</button>
				</div> */}
				</div>
			</div>
			</div>
		)}
		{openModal && (
			<CartModal cart={cart} user={user} openModal={setOpenModal} />
		)}
		{openProductsModal && (
			<VideoProductsModal
			openModal={setOpenProductsModal}
			video={video}
			user={user}
			cart={mobileCart}
			/>
		)}
		{openMenuModal && (
			<HeaderMenuModal user={user} setModal={setOpenMenuModal} />
		)}
		</>
	);
};

const getStaticProps : GetStaticProps = ({params}:any) => withHeader(async (props: any) => 
{
	let video = await getDocument(VIDEO_SETTING.merge({
		document: {
			referencePath: `default/video/documents/${params.id}`,
		},
	}));

    return {
      	props: {
			video : video?.data || null,
      	},
		revalidate : 60,
    };
});

const getStaticPaths = async () =>
{
	const video = await collectionDocument(VIDEO_SETTING.merge({
		perPage : 100,
	}));

	let paths = []

	if(video.collection)
	{
		paths = video.collection.map((item:any) => ({
			params : { id : item.id },
	  	}));  
	}
  
	return { paths, fallback: 'blocking' }
}

export { getStaticProps as GetStaticProps, getStaticPaths as GetStaticPaths, VideoPage}
